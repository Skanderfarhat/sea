#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int magic;

//la fonction qui permet de quitter le shell (Ctrl+D)
void contrD (char *ch,char *res)
{
	int i;
	if (res==NULL)
	{	printf("\n");
		 exit(1);}	
}

//Placer les arguments de la commande entré dans le parseur
void Pa(char *res,char **Parseur)
{	
	int i =0;
	while ((res !=NULL ))//tant que la chaîne n'est pas vide
	{
		//if (i>0){printf("\n");}//cette condition permet d'effacer les lignes vides
		Parseur[i]=strsep(&res,"	 \n");
		i++;}
	Parseur[i-1]=NULL;
	magic=i;
}
//la fonction qui permet de quitter le shell (exit)
void quitter(char **Parseur)
{
	if(strcmp(Parseur[0],"exit")==0)
	{	
		 exit(1);}
}

//fonction pour se deplacer
void cd(char **Parseur,char *Path)
{
	if (strcmp(Parseur[0],"cd")==0)
	{
		chdir(Parseur[1]);
		}
}

// fonction pour lancer la commande entrer
void com(char **Parseur)
{	
	
	pid_t pid;
	
		pid = fork ();
		if(pid==-1){
		perror ("fork");}
		else if(pid==0){
		execvp(Parseur[0],Parseur);
		exit(1);}
		else{
		waitpid (pid, NULL, 0);}
	
	
}

//la redirection , c'est moi qui a fait toute la fonction et y'a que le code de pipe que je l'ai trouvé sur tockoverflow et biensur je l'ai modifié pour qu'il s'adapte avec mon code
void redirection(char **Parseur)
{
	char ** pipa = (char**)malloc (100*sizeof(char)*sizeof(char));
	pid_t pid;
	int a=0;
	int fd;
	int pi=0;
	for (int i=0;i<magic-1;i++)
	{
		if (strcmp(Parseur[i],">")==0)
		{	
			a=1;
			pid=fork();
			if (pid==0)
			{
				fd = open(Parseur[i+1], O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
				dup2(fd,1);
				close(fd);
				Parseur[i]=Parseur[i+1]=NULL;
				execvp(Parseur[0],Parseur);
			}
			else{
			waitpid (pid, NULL, 0);}
			
		}
		else if (strcmp(Parseur[i],"<")==0)
		{	
			a=1;
			pid=fork();
			if (pid==0)
			{
				fd = open(Parseur[i+1], O_RDONLY);
				dup2(fd,0);
				close(fd);
				Parseur[i]=Parseur[i+1]=NULL;
				execvp(Parseur[0],Parseur);
			}
			else{
			waitpid (pid, NULL, 0);}
			
		}
		else if (strcmp(Parseur[i],"2>")==0)
		{	
			a=1;
			pid=fork();
			if (pid==0)
			{
				fd = open(Parseur[i+1], O_WRONLY | O_CREAT , S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
				dup2(fd,2);
				close(fd);
				Parseur[i]=Parseur[i+1]=NULL;
				execvp(Parseur[0],Parseur);
			}
			else{
			waitpid (pid, NULL, 0);}
		}
		else if (strcmp(Parseur[i],"|")==0)
		{
			pi++;
		}
	}
	//verifié si la derniere case du parseur est & si c'est le cas on rappelle le main dans le processus pere pour reexecuter le prog sans attendre et le fils fait le travail derriere
	if (strcmp(Parseur[magic-2],"&")==0)
	{
		Parseur[magic-2]=NULL;
		pid = fork();
		if (pid == 0)
		{
			execvp(Parseur[0],Parseur);
		}
		else
		{
			main();}
	}
	
	//Fonction trouvé sur stockoverflow
	if ( pi!=0)
	{

    int pipefds[2*pi];

    for(int i = 0; i < pi; i++){
        if(pipe(pipefds + i*2) < 0) {
            perror("Couldn't Pipe");
            exit(EXIT_FAILURE);
        }
    }

    int pid;
    int status;

    int j = 0;
    int k = 0;
    int s = 1;
    int place;
    int magicc[10];
    magicc[0] = 0;

   for (int i=0;i<magic-1;i++){
        if(strcmp(Parseur[i], "|")==0){
            Parseur[i] = NULL;
            // printf("args[%d] is now NULL", k);
            magicc[i+1] = i+1;
            s++;
        }
        k++;
    }



    for (int i = 0; i < pi+1; ++i) {
        place = magicc[i];

        pid = fork();
        if(pid == 0) {
            //if not last command
            if(i < pi){
                if(dup2(pipefds[j + 1], 1) < 0){
                    perror("dup2");
                    exit(EXIT_FAILURE);
                }
            }

            //if not first command&& j!= 2*pipes
            if(j != 0 ){
                if(dup2(pipefds[j-2], 0) < 0){
                    perror("dup2");
                    exit(EXIT_FAILURE);
                }
            }

            int q;
            for(q = 0; q < 2*pi; q++){
                    close(pipefds[q]);
            }

            // The commands are executed here, 
            // but it must be doing it a bit wrong          
            if( execvp(Parseur[place], Parseur+place) < 0 ){
                    perror(*Parseur);
                    exit(EXIT_FAILURE);
            }
        }
        else if(pid < 0){
            perror("error");
            exit(EXIT_FAILURE);
        }

        j+=2;
    }

    for(int i = 0; i < 2 * pi; i++){
        close(pipefds[i]);
    }

    for(int i = 0; i < pi + 1; i++){
        wait(&status);
    }
	}//fin de la fonction !
	
	if (a!=1)
	{
		com(Parseur);
	}
}

int main ()
{	
	char * ch = (char*)malloc (100*sizeof(char));
	char ** Parseur = (char**)malloc (100*sizeof(char)*sizeof(char));
	char ss='%';
	char *path=(char*)malloc (30*sizeof(char));
	do
	{	
		getcwd(path,30);//récuperer le répertoire courant
		printf ("%s %c ",path,ss);
		char * res = fgets(ch,100*sizeof(char),stdin);
		contrD(ch,res);
		Pa(res,Parseur);
		quitter(Parseur);
		cd(Parseur,path);
		redirection(Parseur);
		
	}while(1);
	return 0;
}
	
