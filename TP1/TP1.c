#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{	
	pid_t ska;
	int i,j;
	ska = fork();
	
    if (ska == 0) {
		for (i=0; i<10000;i++)
		{printf("b");}
    } 
    else {
		for (j=0; j<10000;j++)
		{printf("a");}
    }
    sleep(10);
}
