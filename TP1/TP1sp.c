#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define taille 10000

int hasard ()
{
	srand(time(NULL));
	return rand()%1001;
}



void settab(int *tab)
{
	int i;
	for (i=0;i<taille;i++)
	{
		tab[i]=hasard();
	}
}

int main()
{	
	int *tab = (int*)malloc((taille*taille)*sizeof(int*));
	int i;
	int c=0;
	pid_t ska;
	
	settab(tab);
	ska = fork();
	
	 if (ska == 0) {
		for (i=0;i<taille/2;i++)
		{
			if (tab[i]==42)
			{printf("%d\n",i);
			c+=1;}
		}
    } 
    else {
		for (i=taille/2;i<taille;i++)
		{
			if (tab[i]==42)
			{printf("%d\n",i);
			c+=1;}
		}
    }
	sleep (10);
	printf("il y'a %d case contenant la valeur 42\n",c);
	return 0;
}

	
