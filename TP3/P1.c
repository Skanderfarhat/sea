#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void * aff(void * a)
{
    printf("Bonjour je suis le thread numéro %d \n", (int)a);
}


int main (int argc , char **argv)
{
	int N = atoi(argv[1]);
	pthread_t thread[N];
	for (int i=0;i<N;i++)
	{
		pthread_create(&thread[i], NULL, &aff, (void*)i);
	}
	
	for (int i=0;i<N;i++)
	{
		pthread_join(thread[i], NULL);
	}
		
		
}
